$(document).on("ready", efectos);


function efectos(){
	$(window).scroll(function(){
		var h = $("#seccion2").height()-120; // Se toma la altura del div principal y se le restan 50 pixeles
		var y = $(window).scrollTop(); // Esto detecta la posicion del scroll y lo guarda en una variable

		if(y < h) { // Se hace la condicion
			$("#menuParallax").css("visibility", "hidden"); // Si la altura es menor o igual al home, se desaparece el menu

		} else {
			$("#menuParallax").css("visibility", "visible"); // De lo contrario, el menu aparece
		}
	});


		/* Servicios */

		$("#psp").on("mouseover", function(){
		$("#scontenido1").show("slow");
		$("#scontenido2").hide("fast");
		$("#scontenido3").hide("fast");
		$("#scontenido4").hide("fast");
		$("#scontenido5").hide("fast");
		$("#scontenido6").hide("fast");
		$("#scontenido7").hide("fast");
		$("#scontenido").css("bottom", "20%");

		});

		$("#psi").on("mouseover", function(){
		$("#scontenido1").hide("fast");
		$("#scontenido2").show("slow");
		$("#scontenido3").hide("fast");
		$("#scontenido4").hide("fast");
		$("#scontenido5").hide("fast");
		$("#scontenido6").hide("fast");
		$("#scontenido7").hide("fast");	
		$("#scontenido").css("bottom", "25%");
		});

		$("#psa").on("mouseover", function(){
		$("#scontenido1").hide("fast");
		$("#scontenido2").hide("fast");
		$("#scontenido3").show("slow");
		$("#scontenido4").hide("fast");
		$("#scontenido5").hide("fast");
		$("#scontenido6").hide("fast");
		$("#scontenido7").hide("fast");
		$("#scontenido").css("bottom", "20%");

		});

		$("#pstm").on("mouseover", function(){
		$("#scontenido1").hide("fast");
		$("#scontenido2").hide("fast");
		$("#scontenido3").hide("fast");
		$("#scontenido4").show("slow");
		$("#scontenido5").hide("fast");
		$("#scontenido6").hide("fast");
		$("#scontenido7").hide("fast");
		$("#scontenido").css("bottom", "30%");

		});

		$("#xh").on("mouseover", function(){
		$("#scontenido1").hide("fast");
		$("#scontenido2").hide("fast");
		$("#scontenido3").hide("fast");
		$("#scontenido4").hide("fast");
		$("#scontenido5").show("slow");
		$("#scontenido6").hide("fast");
		$("#scontenido7").hide("fast");
		$("#scontenido").css("bottom", "15%");

		});

		$("#ac").on("mouseover", function(){
		$("#scontenido1").hide("fast");
		$("#scontenido2").hide("fast");
		$("#scontenido3").hide("fast");
		$("#scontenido4").hide("fast");
		$("#scontenido5").hide("fast");
		$("#scontenido6").show("slow");
		$("#scontenido7").hide("fast");
		$("#scontenido").css("bottom", "20%");

		});

		$("#ds").on("mouseover", function(){
		$("#scontenido1").hide("fast");
		$("#scontenido2").hide("fast");
		$("#scontenido3").hide("fast");
		$("#scontenido4").hide("fast");
		$("#scontenido5").hide("fast");
		$("#scontenido6").hide("fast");
		$("#scontenido7").show("slow");
		$("#scontenido").css("bottom", "20%");

		});
			
		$(window).scroll(function(){
			var a = $("#seccion2").height() + $("#imensaje1").height();
			var b = a + $("#seccion3").height();
			var c = b + $("#seccion4").height();
			var d = c + $("#seccion5").height(); 
			var e = d + $("#seccion6").height()+1000; 
			var f = e + $("#seccion7").height(); 

			var h = $(window).scrollTop(); 


		if(h == a || h < b) { 
			$("#enlace2").css({ 
				"border": ".1em solid #FFF",
				"border-radius": ".5em",
				"background": "#FFF",
				"color":"#000"
		})
			$("#enlace3").css({ 
				"border": "none",
				"background": "#000",
				"color":"#FFF"
		})
			$("#enlace4").css({ 
				"border": "none",
				"background": "#000",
				"color":"#FFF"
		})
			$("#enlace5").css({ 
				"border": "none",
				"background": "#000",	
				"color":"#FFF"
		}) 

			$("#enlace6").css({ 
				"border": "none",
				"background": "#000",
				"color":"#FFF"
		})

		/*$("#enlace2").on("click", function(){ */
 		 $("#enlace2").addClass("menu");
 		 $("#enlace3").removeClass("menu");
 		 $("#enlace4").removeClass("menu");
 		 $("#enlace5").removeClass("menu");
 		 $("#enlace6").removeClass("menu");
			

		} 

		else if(h == b || h < c) {
			$("#enlace3").css({ 
				"border": ".1em solid #FFF",
				"border-radius": ".5em",
				"background": "#FFF",
				"color":"#000"
		})
			$("#enlace2").css({ 
				"border": "none",
				"background": "#000",
				"color":"#FFF"
		})
			$("#enlace4").css({ 
				"border": "none",
				"background": "#000",
				"color":"#FFF"
		})
			$("#enlace5").css({ 
				"border": "none",
				"background": "#000",
				"color":"#FFF"
		})

			$("#enlace6").css({ 
				"border": "none",
				"background": "#000",
				"color":"#FFF"
		})

		 $("#enlace2").removeClass("menu");
 		 $("#enlace3").addClass("menu");
 		 $("#enlace4").removeClass("menu");
 		 $("#enlace5").removeClass("menu");
 		 $("#enlace6").removeClass("menu");
		}

		else if(h == c || h < d) { 
			$("#enlace4").css({ 
				"border": ".1em solid #FFF",
				"border-radius": ".5em",
				"background": "#FFF",
				"color":"#000"
		})
			$("#enlace2").css({ 
				"border": "none",
				"background": "#000",
				"color":"#FFF"
		})
			$("#enlace3").css({ 
				"border": "none",
				"background": "#000",
				"color":"#FFF"
		})
			$("#enlace5").css({ 
				"border": "none",
				"background": "#000",
				"color":"#FFF"
		})

			$("#enlace6").css({ 
				"border": "none",
				"background": "#000",
				"color":"#FFF"
		})

		 $("#enlace2").removeClass("menu");
 		 $("#enlace3").removeClass("menu");
 		 $("#enlace4").addClass("menu");
 		 $("#enlace5").removeClass("menu");
 		 $("#enlace6").removeClass("menu");

		}

		else if(h == d || h < e -100) { 
			$("#enlace5").css({ 
				"border": ".1em solid #FFF",
				"border-radius": ".5em",
				"background": "#FFF",
				"color":"#000"
		})
			$("#enlace2").css({ 
				"border": "none",
				"background": "#000",
				"color":"#FFF"
		})
			$("#enlace3").css({ 
				"border": "none",
				"background": "#000",
				"color":"#FFF"
		})
			$("#enlace4").css({ 
				"border": "none",
				"background": "#000",
				"color":"#FFF"
		})

		$("#enlace6").css({ 
				"border": "none",
				"background": "#000",
				"color":"#FFF"
		})

		 $("#enlace2").removeClass("menu");
 		 $("#enlace3").removeClass("menu");
 		 $("#enlace4").removeClass("menu");
 		 $("#enlace5").addClass("menu");
 		 $("#enlace6").removeClass("menu");

		}   

		else if(h == e || h < f) { 
			$("#enlace6").css({ 
				"border": ".1em solid #FFF",
				"border-radius": ".5em",
				"background": "#FFF",
				"color":"#000"
			})
			$("#enlace2").css({ 
				"border": "none",
				"background": "#000",
				"color":"#FFF"
			})
			$("#enlace3").css({ 
				"border": "none",
				"background": "#000",
				"color":"#FFF"
			})
			$("#enlace4").css({ 
				"border": "none",
				"background": "#000",
				"color":"#FFF"
			})
			$("#enlace5").css({ 
				"border": "none",
				"background": "#000",
				"color":"#FFF"
			})

			$("#enlace2").removeClass("menu");
			$("#enlace3").removeClass("menu");
			$("#enlace4").removeClass("menu");
			$("#enlace5").removeClass("menu");
			$("#enlace6").addClass("menu");
		}   
	});

}

function obscurecer(){
	document.getElementById('luz').style.display='inline-block';
	document.getElementById('fade').style.display='block';
}

function aclarar(){
	var iframe = document.getElementsByTagName("iframe")[0].contentWindow;
    iframe.postMessage('{"event":"command","func":"stopVideo","args":""}','*');
	document.getElementById('luz').style.display='none';
	document.getElementById('fade').style.display='none';	
}