<?php
	$iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
	$android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
	$palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
	$berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
	$ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
	 
	if ($iphone || $android || $palmpre || $ipod || $berry == true) {
		header('Location: http://www.bhacking.com/m');
	}

	ini_set('display_errors', 1);
	require_once('TwitterAPIExchange.php');

	/** Set access tokens here - see: https://dev.twitter.com/apps/ **/
	$settings = array(
	    'oauth_access_token' => "129689742-7prlccDxb8kIhZmc1ERHCVVIipxljpICoHztiu0m",
	    'oauth_access_token_secret' => "dGl1cSTnf8c8IWu3zUJuY9YlCDG1tTTAVzWBDH1FQ",
	    'consumer_key' => "6QZVo4ThtV3k2qEYUE9g",
	    'consumer_secret' => "gZqYM73UPbqlmzaJhgtBMhpdIJsmOhMxFc9pLks53zk"
	);

	$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
	$getfield = '?screen_name=bh4ck1ng&count=1';
	$requestMethod = 'GET';

	$twitter = new TwitterAPIExchange($settings);
	$response = $twitter->setGetfield($getfield)
	                    ->buildOauth($url, $requestMethod)
	                    ->performRequest();
	$tweets = json_decode($response, true);
?>
<!DOCTYPE html>
<html lang="es-mx">
<head>
	<meta charset="utf-8"/>
	<title>BHacking</title>
	<meta name="description" content="BHacking">
	<link rel="stylesheet" href="css/estilos.css"/>
	<link rel="stylesheet" href="css/responsiveslides.css"/>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/style_common.css" />
    <link rel="stylesheet" type="text/css" href="css/style10.css" />
	<link rel="shortcut icon" href="img/bh.ico">
	<!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <! [endif] -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<script>
		window.jQuery || document.write("<script src='js/jquery.min.js'><\/script>");
	</script>
	<script src="js/jquery.anchor.js"></script>
	<script src="js/efectos.js"></script>
	<script src="js/responsiveslides.min.js"></script>
	<script type="text/javascript" src="js/servicios/modernizr.custom.69142.js"></script>
	<script>
	    $(function () {
	      $("#slider").responsiveSlides({
	        auto: false,
	        pager: false,
	        nav: true,
	        speed: 500,
	        namespace: "contenedor"
	      });
	    });
  	</script>
  	<noscript>
		<link rel="stylesheet" type="text/css" href="css/nojs.css" />
	</noscript>
</head>
<body>
	<!--<script>
		function deteccion(){
		    if(navigator.platform == 'iPhone' || navigator.platform == 'iPod') { 
		    	location.replace("m/");
		    }
		}
		window.onload = setTimeout("deteccion();",0000);
	</script>-->
	<section id="seccion1">
		<h1>bHacking</h1>
		<p>Seguridad.Cre@tiva</p>
		<br/>
		<br/>
		<br/>
		<h3>Compatible con:</h3>
		<img class="chrome" src="img/chromelogo.png"/>
		<img class="firefox" src="img/firefox_logo.png"/>
		<img class="safari" src="img/Safari.png"/>
		<img class="opera" src="img/Opera.png"/>
		<a href="#seccion2" class="anchor-link cambio" id="activar"><img id="flecha" src="img/arrowbottom.png"></a>
	</section>

		<section id="imensaje1">
		<p>Desire is what compeels people to create something new</p>
		</section>

		<section id="seccion2">
		<nav id="menuParallax">
				<ul class="cmenu">
					<li id="link1"><a id="enlace1"  href="#seccion1" class="anchor-link cambio">bHacking</a></li>
					<li class="link"><a id="enlace2" href="#seccion2" class="anchor-link cambio">Quiénes Somos</a></li>
					<li class="link"><a id="enlace3" href="#seccion3" class="anchor-link cambio">Servicios</a></li>
					<li class="link"><a id="enlace4" href="#seccion4" class="anchor-link cambio">Academia</a></li>
					<li class="link"><a id="enlace5" href="#seccion5" class="anchor-link cambio">Colaboradores</a></li>
					<li class="link"><a id="enlace6" href="#seccion6" class="anchor-link cambio">Contacto</a></li>
				</ul>
				</nav>
	<article id="qsomos">
		
		<article id="ilogo">
			<img src="img/prop2.jpg">
		</article>

		<article id="mision" class="cambio somos" > 
			<h2>Misión</h2>
			<p>
			Asesorarte creativa y activamente, respecto a las principales debilidades en materia de seguridad digital  que pueden afectarte o dañarte según tu tipo de persona o empresa.
			</p>
			<hr />

			<h2>Visión</h2>		
			<p>
			Ser una compañía de referencia en México, preferentemente por su contribución a la cultura de seguridad, su presencial social y su propuesta educativa.
		    </p>

			<hr/>
			<h2>Valores</h2>
			<p>
				<ul class="cvalores">
					<li>Ser auténticos: Amar lo que eres y lo que haces</li>
					<li>Ser responsables: Cumplirme lo que me prometo</li>
					<li>Ser Creactivo: Creatividad puesta en acción</li>
				</ul>
				<br/>
			</p>
		</article>
		
		<article id="itexto">
				<blockquote>
					<p id="sc">Por que sabemos lo importante que es sentirte seguro.
						<br />
						<p id="creative">Don't worry </p>
						<p>bHacking</p>
					</p>
				</blockquote>
		</article>
	</article>

	</section>
	
	<section id="imensaje2">
		<p>Simplicity is the ultimate sophistication</p>
	</section>	
	
	<section id="seccion3">
		<div class="container">		
			<ul id="sg-panel-container">
				<li data-w="50" data-h="100">
					<img title="Hacking" src="images/hacking.jpg" data-rotate-x="50" />
					<img id="tituloNegro" title="Análisis Forense Digital" src="images/forense.jpg" data-rotate-x="50" data-translate-z="-700" />
				</li>
				<li data-w="35" data-h="100">
					<img title="Hacking" src="images/hacking.jpg" data-rotate-x="50" />
					<div data-translate-z="-500" >
						<div class="sg-content">
							<h3>Prueba de Seguridad Externas</h3>
							<p>Evaluación de los niveles de seguridad de la organización a cara a los
								servicios publicados en Internet.</p>
							<br/>
							<h3>Pruebas de Seguridad Internas</h3>
							<p>Evaluación de los niveles de seguridad conectados a la red interna de la 
								organización.</p>
							<br/>
							<h3>Pruebas de Seguridad Aplicativas</h3>
							<p>Evaluación de niveles de seguridad de aplicativos que soportan procesos organizacionales.</p>
							<br/>
							<h3>Análisis de Código</h3>
							<p>Evaluación de los niveles de seguridad para los desarrollos internos de la organización.</p>
							<br/>
							<h3>Pruebas de Seguridad Tecnología Móvil</h3>
							<p>Evaluación de los niveles de seguridad sobre tecnología emergentes para empresas.</p>
							<br/>	
							<h3>Xtreme Hacking</h3>
							<p>Evaluación de los niveles de seguridad desde la perspectiva maliciosa de un hacker con el fin de obtener</p><p>una visión real de los niveles de seguridad necesarios para la organización.</p>
						</div>
					</div>
				</li>
				</li>
				<li data-w="70" data-h="100">
					<div data-translate-z="-500" >
						<div class="sg-content" id="serviciosPrueba">
							<h3>Investigaciones Digitales</h3>
							<p>-Robo de Secretos Industriales</p>
							<p>-Robo de propiedad Intelectual</p>
							<p>-Fraude Financiero</p>
							<p>-Abuso de Confianza</p>
							<p>-Amenazas y Difamación</p>
							<p>-Recuperación de Información</p>
							<p>-Espionaje a traves de TI</p>
							<p>-Violación de Sistemas de Información</p>
							<br/>
							<h3>Investigaciones en Medios Sociales</h3>
							<p>-Cloud Computing (Dropbox, Google, etc)</p>
							<p>-Chats (MSN, ICQ, Adium, etc)</p>
							<p>-Correo Electrónico (WebMail y Clientes)</p>
							<p>-P2P (Frostwire, Limeware, etc)</p>
							<p>-Redes Sociales(Facebook, Twitter, etc)</p>
							<p>-Actividades y Registros Web(Chrome, IE, etc)</p>
							<br/>
							<h3>Recuperación de Contraseñas</h3>
							<p>-Sistema de Escritorio: Windows, Linux, Unix.</p>
							<p>-Sistemas Móviles: iOS, Blackberry.</p>
							<p>-Unidades Extraibles: USB, SdCards, etc.</p>
							<p>-Archivos tipo MS Office.</p>
							<p>-Archivos tipo Adobe Acrobat PDF.</p>
							<p>-Otros tipos de archivos: PGP, PFX.</p>
							<p>-ZIP, RAR, ACE, ARJ entre otros...	</p>
							<br/>
							<h3>Recuperación de Información</h3>
							<p>-Recuperación de datos borrados.</p>
							<p>-Revisión de slack spaces.</p>
							<p>-Sistemas de Escritorio: Windows, Linux, Unix.</p>
							<p>-Sistemas Móviles: iOS, Blackberry.</p>
							<p>--Unidades Extraibles: USB, SdCards, etc.</p>
						</div>
					</div>
					<img id="tituloNegro" title="Asesoría Forense" src="images/forense.jpg" data-rotate-x="50" />
				</li>
			</ul>
		</div>
		<script type="text/javascript" src="js/servicios/jquery.transit.min.js"></script>
		<script type="text/javascript" src="js/servicios/jquery.imagesloaded.js"></script>
		<script type="text/javascript" src="js/servicios/jquery.ba-dotimeout.min.js"></script>
		<script type="text/javascript" src="js/servicios/jquery.gridgallery.js"></script>
		<script type="text/javascript">	
			$(function() {
				$( '#sg-panel-container' ).gridgallery();
			});
		</script>
		
	</section>
	
	<section id="imensaje3">
		<p>Gurus provide ready made solutions, but educators provide ways that one can find solutions for oneself</p>
	</section>	
	
	
	<div id="seccion4">
        <div class="main">
        	<div>
        		<p id="academia">La creatividad puesta en acción por parte del equipo, ha podido dar como resultado la transferencia de conocimiento en diferentes instituciones educativas en las que se participa así como también, difusión de ciencia a través de cursos online.</p>
        	</div>
            <div class="view view-tenth">
                <img src="img/Wireless Hacking.jpg" />
                <div class="mask">
                    <h2>Wireless Hacking 1/2 WEP</h2>
                    <p>Muestra el proceso de hackeo de redes inalámbricas sobre mecanismos de cifrado WEP a través del uso de herramientas OpenSource.</p>
                    <a href="http://www.youtube.com/watch?v=6Gvb2d0Rudk"  target="_blank" class="info">Ver video</a>
                </div>
            </div>
            <div class="view view-tenth">
                <img src="img/Invitacion.jpg" />
                <div class="mask">
                    <h2>Wireless Hacking 2/2 WPA</h2>
                    <p>Muestra el proceso de hackeo de redes inalámbricas sobre mecanismos de cifrado WPA a través del uso de herramientas OpenSource.</p>
                    <a href="http://www.youtube.com/watch?v=8YI0Vo5dhi0"  target="_blank" class="info">Ver video</a>
                </div>
            </div>
            <div class="view view-tenth">
                <img src="img/DataRecovery.jpg" />
                <div class="mask">
                    <h2>Data Recovery</h2>
                    <p>Muestra el proceso de recuperación de datos a través del uso de herramientas OpenSource aún cuando se hayan borrado tus datos.</p>
                    <a href="http://www.youtube.com/watch?v=hZf07vi80Es"  target="_blank" class="info">Ver video</a>
                </div>
            </div>
            <div class="view view-tenth">
                <img src="img/Carving.jpg" />
                <div class="mask">
                    <h2>Data Recovery II (Carving)</h2>
                    <p>Muestra el proceso de recuperación de datos a través del uso de herramientas OpenSource aún cuando se hayan sobreescrito tus datos.</p>
                    <a href="http://www.youtube.com/watch?v=vgbEJqPMqK4"  target="_blank" class="info">Ver video</a>
                </div>
            </div>
            <div class="view view-tenth">
                <img src="img/curso-sniffing.png" />
                <div class="mask">
                    <h2>MITM - Sniffing 1/3</h2>
                    <p>Muestra el resultado de un ataque Man in the Middle así como sus vectores de hackeo.</p>
                    <a href="http://www.youtube.com/watch?v=8Wvh_EypTVo"  target="_blank" class="info">Ver video</a>
                </div>
            </div>
            <div class="view view-tenth">
                <img src="img/curso 2 prerouting.jpg" />
                <div class="mask">
                    <h2>MITM - Sniffing 2/3</h2>
                    <p>Muestra el resultado de un ataque Man in the Middle así como sus vectores de hackeo.</p>
                    <a href="http://www.youtube.com/watch?v=U6AdlP9QZ3U"  target="_blank" class="info">Ver video</a>
                </div>
            </div>
            <div class="view view-tenth">
                <img src="img/curso 3 - robo de contrasenas.jpg" />
                <div class="mask">
                    <h2>MITM - Robo de PW 3/3</h2>
                    <p>Muestra el resultado de un ataque Man in the Middle así como sus vectores de hackeo.</p>
                    <a href="http://www.youtube.com/watch?v=V-kEMYYXoBQ"  target="_blank" class="info">Ver video</a>
                </div>
            </div>
            <div class="view view-tenth">
                <img src="img/securityassessment.jpg" />
                <div class="mask">
                    <h2>Security Assessment</h2>
                    <p>Muestra el resultado de una prueba de seguridad interna y la forma en que se lleva a cabo.</p>
                    <a href="http://www.youtube.com/watch?v=lwExA723AQU"  target="_blank" class="info">Ver video</a>
                </div>
            </div>
            <div class="view view-tenth">
                <img src="img/password craking.jpg" />
                <div class="mask">
                    <h2>Password Cracking</h2>
                    <p>Muestra el proceso de recuperación de usuarios y contraseñas en sistemas Windows y Linux a través</p>
                    <a href="http://www.youtube.com/watch?v=E0Xqs957M88"  target="_blank" class="info">Ver video</a>
                </div>
            </div>
            <div class="view view-tenth">
                <img src="img/ANALISIS FORENSE.jpg" />
                <div class="mask">
                    <h2>Análisis Forense "Lo que oculta tu red"</h2>
                    <p>Muestra generales de interpretación en materia forense respecto a archivos tipo .cap provenientes de una herramienta tipo sniffer como Wireshark. </p>
                    <a href="http://www.youtube.com/watch?v=WrizbgqPh7k"  target="_blank" class="info">Ver video</a>
                </div>
            </div>
            <div class="view view-tenth">
                <img src="img/DoS.png" />
                <div class="mask">
                    <h2>DoS - Denegación de Servicio</h2>
                    <p>Muestra la forma en la que se lleva a cabo un ataque tipo DoS (Denegación de Servicio) y sus implicaciones. </p>
                    <a href="http://www.youtube.com/watch?v=E0Xqs957M88"  target="_blank" class="info">Ver video</a>
                </div>
            </div>
        </div>
    </div>

	<section id="imensaje4">
		<p>As you act, you can find people to come along with you</p>
	</section>

	<section id="seccion5">	
		
		<article class="logos">
			<a href="http://www.daat.mx/" target="_blank"><img src="img/logoDaat.jpg"></a>
		</article>

		<article class="it">
			<a href="http://www.itlawyers.com.mx/" target="_blank"><img src="img/itlawyers.png"></a>
		</article>

		<article class="bh">
			<img src="img/prop2.jpg"/>
		</article>

		<article class="artd">
			<img src="img/artdlogo.jpg">
		</article>

		<article class="inelec">
			<a href="http://www.inelecsoluciones.com.mx/" target="_blank"><img src="img/INELEC.png"></a>
		</article>
	</section>			

		<section id="imensaje5">
			<article id="contenedor4">
				<a id="follow" href="https://twitter.com/bh4ck1ng" class="twitter-follow-button" data-show-count="true" data-lang="en" data-size="large">Follow</a>
				<script>
					!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];
						if(!d.getElementById(id)){
							js=d.createElement(s);
							js.id=id;js.src="//platform.twitter.com/widgets.js";
							fjs.parentNode.insertBefore(js,fjs);
						}
					}
						(document,"script","twitter-wjs");
				</script>
				<?php
					echo "<p id='tweet'>".utf8_decode($tweets[0]['text'])."</p>";
				?>
			</article>
		</section>	
	
			<section id="seccion6">
		
			<article id="titulo1">
				<p id="contacto1">Contactanos en:</p>
				<p id="redess">Redes Sociales
				<br>
				<a class="links facebook" href="https://www.facebook.com/pages/BHacking/309082399205489?fref=ts" target="_blank"><img class="facebook" src="img/logo_facebook.png"/>  bHacking</a> 
				<br>
				<a class="links twitter"href="https://twitter.com/bh4ck1ng" target="_blank"><img class="twitter" src="img/Twitter-Logo.png"/> @bh4ck1ng</a>
				<br>
				<a class="links youtube" href="http://www.youtube.com/user/bh4ck1ng" target="_blank"><img class="youtube" src="img/youtube.png"> bh4ck1ng</a> 
				</p>
				<p id="contacto2">contacto@bhacking.com</p>
				<footer>
					<section>
					<article id="desarrollado">Desarrollado por BHACKING&#174</article>
					</section>
				</footer>
			</article>
		</section>
		<section id="seccion7">

			<br>
			<br>
		</section>
</body>
</html>